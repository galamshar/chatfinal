package kz.aitu.chat.controller;


import kz.aitu.chat.RequestModels.LoginRequest;
import kz.aitu.chat.RequestModels.RegisterRequest;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private AuthService authService;

    @PostMapping("login")
    public ResponseEntity<?> login(@RequestBody LoginRequest auth) throws Exception {
        return ResponseEntity.ok(authService.Login(auth.getLogin(), auth.getPassword()));
    }

    @PostMapping("register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest auth) throws Exception {
        return ResponseEntity.ok(authService.Register(auth.getLogin(), auth.getPassword(), auth.getName()));
    }

    @PostMapping("getUserByToken")
    public ResponseEntity<?> getUserByToken(@RequestBody UUID token) throws Exception {
        return ResponseEntity.ok(authService.getUserByToken(token));
    }
}
