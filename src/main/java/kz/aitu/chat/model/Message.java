package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private Long chatId;
    private String text;
    private Long created_timestamp;
    private Long updated_timestamp;
    private boolean isDelivered;
    private Long deliveredTimestamp;
    private boolean isRead;
    private Long readTimestamp;
    private String messageType;

    public void setText(String text) {
        this.text = text;
    }

    public void setUpdated_timestamp(Long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    public void setDeliveredTimestamp(Long deliveredTimestamp) {
        this.deliveredTimestamp = deliveredTimestamp;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public void setReadTimestamp(Long readTimestamp) {
        this.readTimestamp = readTimestamp;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getChatId() {
        return chatId;
    }

    public String getText() {
        return text;
    }

    public Long getCreated_timestamp() {
        return created_timestamp;
    }

    public Long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public Long getDeliveredTimestamp() {
        return deliveredTimestamp;
    }

    public boolean isRead() {
        return isRead;
    }

    public Long getReadTimestamp() {
        return readTimestamp;
    }

    public Long getId() {
        return id;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageStatus) {
        this.messageType = messageStatus;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @PrePersist
    protected void onCreate() {
        created_timestamp = updated_timestamp = new Date().getTime();
        isDelivered = false;
        isRead = false;
    }

    @PreUpdate
    protected void onUpdate() {
        updated_timestamp = new Date().getTime();
    }
}
