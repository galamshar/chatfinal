drop table if exists auth;
create table auth
(
    id serial,
    login varchar(64),
    password varchar(64),
    last_login_timestamp bigint,
    user_id bigint,
    token uuid
);

drop table if exists users;
create table users
(
    id   serial,
    name varchar(255)
);

drop table if exists chat;
create table chat
(
    id serial,
    name varchar(255)
);

drop table if exists message;
create table message
(
    id serial,
    user_id bigint,
    chat_id bigint,
    text varchar(255),
    created_timestamp bigint,
    updated_timestamp bigint,
    is_delivered boolean,
    delivered_timestamp bigint,
    is_read boolean,
    read_timestamp bigint,
    message_type varchar(255)
);

drop table if exists participant;
create table participant
(
    id serial,
    user_id bigint,
    chat_id bigint
);

